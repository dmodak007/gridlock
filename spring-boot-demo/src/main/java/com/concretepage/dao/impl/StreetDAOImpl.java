package com.concretepage.dao.impl;

import com.concretepage.dao.StreetDAO;
import com.concretepage.entity.Street;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class StreetDAOImpl implements StreetDAO {

	@PersistenceContext	
	private EntityManager entityManager;	

	@Override
	public Street getArticleById(int articleId) {
		return entityManager.find(Street.class, articleId);
	}

	@Override
	public List<Street> getStreetsFromRoute(String route) {

		String query = "SELECT id, startLat, startLng, endLat, endLng, duration from streets where id in ( " + route + " ) order by Field ( id, " + route + " )";
		Query q = entityManager.createNativeQuery(query);
		List<Object[] > resultList = q.getResultList();

		List<Street> streetList = new ArrayList<>();
		for(Object[] result: resultList){
			Street street = new Street();
			street.setId(Long.parseLong(result[0].toString()));
			street.setStartLat(Double.parseDouble(result[1].toString()));
			street.setStartLng(Double.parseDouble(result[2].toString()));
			street.setEndLat(Double.parseDouble(result[3].toString()));
			street.setEndLng(Double.parseDouble(result[4].toString()));
			street.setDuration(Long.parseLong(result[5].toString()));

			streetList.add(street);
		}

		return streetList;
	}

}
