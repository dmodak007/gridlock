package com.concretepage.dao.impl;

import com.concretepage.dao.RouteDAO;
import com.concretepage.entity.Route;
import com.concretepage.entity.Street;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class RouteDAOImpl implements RouteDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Route getRouteById(Long routeId) {
		return entityManager.find(Route.class, routeId);
	}

	@Override
	public void updateRoute(Long id, Long actualTime) {

		String query = "update routes set actual_time = " + actualTime + " where id = " + id;
		Query q = entityManager.createNativeQuery(query);
		int updateId = q.executeUpdate();

	}
}
