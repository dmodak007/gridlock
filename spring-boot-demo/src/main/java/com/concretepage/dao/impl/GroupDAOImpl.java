package com.concretepage.dao.impl;

import com.concretepage.dao.GroupDAO;
import com.concretepage.dao.StreetDAO;
import com.concretepage.entity.Group;
import com.concretepage.entity.Street;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class GroupDAOImpl implements GroupDAO {

	@PersistenceContext	
	private EntityManager entityManager;	

	@Override
	public Group getGroupById(Long groupId) {
		return entityManager.find(Group.class, groupId);
	}

}
