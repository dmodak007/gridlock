package com.concretepage.domain;


public class TimeSlot {

    private Integer startTime;

    private Integer endTime;

    public TimeSlot() {
        this.startTime = 0;
        this.endTime = 0;
    }

    public TimeSlot(Integer startTime, Integer endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }
}
