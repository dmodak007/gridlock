package com.concretepage.service;

import com.concretepage.entity.Route;

/**
 * Created by 300002291 on 17/06/17.
 */
public interface RouteService {

    public Route getRouteById(int routeId);

    public void startAlgorithm(Long groupId);

}
