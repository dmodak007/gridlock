# Gridlock Hackathon
## RescueMate

Bangalore traffic is primarily due to unmatched pace of population growth & infrastructural growth. Massive influx of employers & employees over the past few years has increased congestion during office reach & exit hours.
Office commuters usually have a similar path to travel every day. Our solution to this Bangalore menace is Rescue Mate – schedule vehicles to minimize congestion. 

The aim of Rescue Mate is to intelligently allocate an approximate time to commuters to start their rides from source location such that wait time of every individual is minimized & #vehicles at any instance do not exceed carrying capacity. Rescue Mate can be effective only if we have all commuters’ data of their location, destination & acceptable time to reach. Our platform collects these user data to schedule the start time of riders. 

To make the system more robust, we intend to collect travel data through cab booking services, hyperlocal vendors, bus schedule etc. Reach of the application is another aspect in the success of this platform. An integration with a platform like Google Maps, Flipkart, Uber etc. will benefit in achieving desired volume of data needed for processing.

### DEMO Links 
[1] Worst case situation


http://35.154.187.149/gridlock/?flow=unprocessed&group=6 


[2] Optimized situation


http://35.154.187.149/gridlock/?flow=processed&group=6